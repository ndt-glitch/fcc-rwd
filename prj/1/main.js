const mkItem = ({ site, jaName, title, desc }) => {
	const li = document.createElement('li')
	const a = document.createElement('a')

	a.setAttribute('class', 'title')
	a.setAttribute('target', '_blank')
	a.setAttribute('href', 'http://' + site)
	a.setAttribute('title', jaName)

	const titleNode = document.createTextNode(title)
	a.appendChild(titleNode)
	li.appendChild(a)

	const bodyNode = document.createTextNode(desc)
	li.appendChild(bodyNode)

	return li
}

const mkItems = data => {
	return data.map(mkItem)
}

;(async x => {
	try {
		const data = await new Loader('prj-1').loadJSON()
		const render = new Render('#tribute-info')
		const items = mkItems(data)
		render.load(...items).start()
	} catch (e) {
		throw e
	}
})()

// JSON codepen.io/dangtu/pen/MqXEmb

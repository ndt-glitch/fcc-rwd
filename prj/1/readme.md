# Build a Tribute Page
This is what it should look like from the client side.

![thumbnail](https://cdn.glitch.com/d57d58b2-47f5-4e08-b167-e37e516ac149/prj1.png)

The thumbnail was rendered in Firefox Quantum 68.

## Content?
Since the objective doesn't strict about the content, I decided to have fun for a bit. The content theme is all about anime + lesbian.

## Unpassable?
That because I use the HTML5 standard for the project while FFC tester only support the old HTML (4 or lower?). Already report it, but seems like nothing will be changed soon.

## Changelog
- Footer part
	- HTML: adding the back link to project launcher (homepage)
	- CSS: the border was moved from `p:last-child` to `footer nav` changing along some property of other sibling elements.


# Legacy assets
- Codepen ID: [OoZxJe](https://codepen.io/dangtu/pen/OoZxJe)
fetch('/db/prj-3.json')
	.then(res => res.json())
	.then(db => {
		const veil = $('#veil')
		setTimeout(
			() => {
				veil.classList.add('liftUp')
				veil.remove()
			},
			765
		)

		const entry = new News(db.news)
		const book = new Manga(db.manga)

		entry.print()
		book.print()
	})

const video = new Video();
video.fetchYoutube('9Tua7jvQgUs')
	.setCaption('第2弾トレーラー')
	.ready();
# Product Landing Page
This is what it should look like from the client side.

![thumbnail](https://cdn.glitch.com/d57d58b2-47f5-4e08-b167-e37e516ac149/prj3.png)

The thumbnail was rendered in Firefox Quantum 68.


# Legacy assets
- Codepen ID: [zJgGyz](https://codepen.io/dangtu/pen/zJgGyz)
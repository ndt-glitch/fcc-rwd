# Technical Documentation Page
This is what it should look like from the client side.

![thumbnail](https://cdn.glitch.com/d57d58b2-47f5-4e08-b167-e37e516ac149/prj4.png?v=1567215164830)

The thumbnail was rendered in Firefox Quantum 68.


# Legacy assets
- Codepen ID: [RqjGWB](https://codepen.io/dangtu/pen/RqjGWB)
- Database:
    -  http://myjson.com/tskge
    -  https://api.myjson.com/bins/tskge
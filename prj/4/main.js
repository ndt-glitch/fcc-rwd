
const print = db => {
	var nav = new Nav();
	var content = new Content();

	for (var i = 0; i < db.length; i++) {
		let id = convID(db[i].title);
		nav.addItem(id, db[i].title);
		content.addItem(id, db[i].title, db[i].content)
	}
	nav.setEvent();
}

ready(function () {
	fetch('/db/prj-4.json')
		.then(response => {
			if (response.ok) return response.json()
		})
		.then(db => print(db))
		.catch(err => {
			console.error('Some freaking thing happens, E:%s', err)
		});
})
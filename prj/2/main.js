fetch('https://dt-rwd.glitch.me/asset/0/db.json')
	.then(res => res.json())
	.then(db => {
		const content = new Content(db);
		content.draw();

		const nav = new Nav();
		nav.run();
	})
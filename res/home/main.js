;(async x => {
	try {
		const data = await new Loader('site').loadJSON()
		const render = new Render()

		const prjs = projectBoxes(data)
		const info = infoList(data)

		render.load(...prjs).start()
	} catch (e) {
		throw e
	}
})()

const temp = document.querySelector('#project')

const readMeta = data => {
	const path = data.projectPath.replace('{{name}}', data.name)

	const asset = data.assetAddress
		.replace('{{assetID}}', data.assetID)
		.replace('{{version}}', data.version)

	return {
		projectPath: path,
		assetAddress: asset
	}
}

const getLicense = (data, meta) => {
	const name = data.name
	const detail = data.detail.replace('{{name}}', meta.name)
	return { name, detail }
}

const cloneBox = (project, meta) => {
	const box = temp.content.cloneNode(true)

	const link = box.querySelector('.project-link')
	const thumb = box.querySelector('.project-thumb')
	const title = box.querySelector('.project-name')

	link.href = meta.projectPath.replace('{{prjID}}', project.id)
	thumb.src = meta.assetAddress.replace('{{thumbnail}}', project.thumbnail)
	title.innerText = project.name

	return box
}

const projectBoxes = data => {
	const meta = readMeta(data.meta)

	let boxes = []
	for (const project of data.projects) {
		const box = cloneBox(project, meta)
		boxes.push(box)
	}
	return boxes
}

const infoList = data => {
	const author = data.info.author
	const license = getLicense(data.info.license, data.meta)
}

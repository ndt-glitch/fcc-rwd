// codeburst.io/how-to-create-horizontal-scrolling-containers-d8069651e9c6

// https://developers.google.com/youtube/iframe_api_reference

function $(el) {
	return document.querySelector(el);
}

class DB {
	static fetch(db, callback) {
		for (var i = 0; i < db.length; i++) {
			callback(db[i]);
		}
	}
}

class News {
	constructor(news) {
		this.db = news;
		let sect = $('#news');

		this.box = sect.querySelector('.box');
		let temp = sect.querySelector('template');

		this.entry = temp.content.querySelector('article.entry');
		this.date = temp.content.querySelector('.entry.date');
		this.title = temp.content.querySelector('.entry.headline');
		this.content = temp.content.querySelector('.entry.summary');
	}

	static setEvent(item) {
		item.onclick = () => {
			item.querySelector('.entry.summary').classList.toggle('show');
		}
	}

	print() {
		DB.fetch(
			this.db,
			data => {
				let entry, date, title, content;
				entry = document.importNode(this.entry);
				date = document.importNode(this.date);
				title = document.importNode(this.title);

				date.innerHTML = data.date;
				title.innerHTML = data.title;

				entry.appendChild(date);
				entry.appendChild(title);

				if (data.content != null) {
					content = document.importNode(this.content);
					content.innerHTML = data.content;
					entry.appendChild(content);
				}

				this.box.appendChild(entry);

				News.setEvent(entry);
			}
		);
	}
}

class Video {
	constructor() {
		let sect = $('#movie');

		this.box = sect.querySelector('.box');
		let temp = sect.querySelector('template');

		this.cover = temp.content.querySelector('.video-cover');
		this.play = temp.content.querySelector('.video-play');
		this.img = temp.content.querySelector('.video-cover > img');
		this.title = temp.content.querySelector('.video-title');

		this.popup = document.createElement('iframe');
		this.popup.id = 'video';
		this.popup.setAttribute('allow', 'encrypted-media');
		this.popup.setAttribute('allowfullscreen', '')
	}

	addPlayBtn() {
		let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
		let btn = document.createElementNS('http://www.w3.org/2000/svg', 'path');

		svg.classList.add('video-btn');
		svg.setAttribute("viewBox", "0 0 25 25");

		btn.setAttribute('d', 'M10,16.5V7.5L16,12M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z');

		svg.appendChild(btn);

		this.play.appendChild(svg);
	}

	fetchYoutube(id) {
		let cover = 'https://img.youtube.com/vi/' + id + '/maxresdefault.jpg';
		let video = 'https://www.youtube.com/embed/' + id + '?rel=0&amp;showinfo=0';

		this.img.src = cover;
		this.popup.setAttribute('src', video);

		return this
	}

	setCaption(caption) {
		this.title.innerHTML = caption;
		return this
	}

	openYoutube() {
		let modal = null;
		if (!$('#modal')) {
			modal = document.createElement('div');
			modal.setAttribute('id', 'modal');
			document.body.appendChild(modal)
		}
		else {
			modal = $('#modal')
		}

		modal.appendChild(this.popup);
		modal.onclick = e => modal.remove();
	}

	ready() {
		this.addPlayBtn();
		this.box.appendChild(this.cover);
		this.box.appendChild(this.title);

		this.cover.onclick = () => this.openYoutube();
	}
}

class Manga {
	constructor(manga) {
		this.db = manga;
		let sect = $('#book');

		this.box = sect.querySelector('.box');
		let temp = sect.querySelector('template');

		this.book = temp.content.querySelector('div.books');

		this.leftCol = temp.content.querySelector('.books.col.left');
		this.img = temp.content.querySelector('.books.cover');
		this.vol = temp.content.querySelector('.books.vol');

		this.rightCol = temp.content.querySelector('.books.col.right');
		this.title = temp.content.querySelector('.books.title');
		this.desc = temp.content.querySelector('.books.desc');
		this.price = temp.content.querySelector('.books.price');
	}

	print() {
		DB.fetch(
			this.db,
			data => {
				let book, lCol, rCol, img, vol, title, desc, price;
				book = document.importNode(this.book);
				lCol = document.importNode(this.leftCol);
				rCol = document.importNode(this.rightCol);
				img = document.importNode(this.img);
				vol = document.importNode(this.vol);
				title = document.importNode(this.title);
				desc = document.importNode(this.desc);
				price = document.importNode(this.price);

				img.src = data.cover;
				vol.innerHTML = '第' + data.vol + '巻';
				title.innerHTML = data.title;
				desc.innerHTML = data.desc;
				price.innerHTML = '定価' + data.price + '円＋税';

				lCol.appendChild(img);
				lCol.appendChild(vol);

				rCol.appendChild(title);
				rCol.appendChild(desc);
				rCol.appendChild(price);

				book.appendChild(lCol);
				book.appendChild(rCol);

				this.box.appendChild(book);
			}
		);
	}
}
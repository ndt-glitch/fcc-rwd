const ready = callback => {
	window.onload = () => callback();
}

const convID = txt => {
	//let regex = /[\? \! \& \']/g; //Stupid test require keeping it
	if (typeof txt === 'string') {
		txt = txt.replace(/\s/g, '_');
		// txt.replace(regex, '')//remove ? ! ' &
		return txt
	}
	else {
		return null
	}
}

class Screen {
	constructor() {
		this.view = document.body; //viewport
	}

	width() { return this.view.clientWidth }
	height() { return this.view.clientHeight }

	isLandscape() { return this.width > this.height }
}

class Nav {
	constructor() {
		this.module = document.querySelector('#navbar');
		this.body = this.module.querySelector('#navbody');
		this.btn = document.querySelector('#menu');
		this.temp = document.querySelector('#navtemp');

		this.lnk = this.temp.content.querySelector('a.nav-link');

		this.view = new Screen();
	}

	addItem(id, name) {
		let item = document.importNode(this.lnk);

		item.setAttribute('href', '#' + id);
		item.innerHTML = name;

		this.body.appendChild(item);

		item.onclick = () => {
			if (this.view.width() <= 690 && this.view.isLandscape()) {
				this.navClose();
				this.btnToggle()
			}
		}

		return this
	}

	btnToggle() {
		this.btn.classList.toggle('open');
		this.btn.classList.toggle('close')
	}

	isNavOpen() { return this.module.style.left == '0px' } // will have trouble when comparing with number only (< / >)
	navOpen() { this.module.style.left = 0 }
	navClose() { this.module.style.left = null }//using the main style, a bit risky

	toggle() {
		console.log(this.module.style.left);
		console.log(this.isNavOpen());
		if (this.isNavOpen()) this.navClose();
		else this.navOpen();

		this.btnToggle();
	}

	setEvent() {
		this.btn.onclick = () => this.toggle();
	}
}

class Content {
	constructor() {
		this.module = document.querySelector('main');
		this.temp = document.querySelector('#sectemp');

		this.sec = this.temp.content.querySelector('.main-section');
		this.header = this.temp.content.querySelector('header');
		this.lnk = this.temp.content.querySelector('header > a');
		this.title = this.temp.content.querySelector('header > h1');
		this.body = this.temp.content.querySelector('article');
	}

	static parseHTML(raw) {
		let doc = (new DOMParser()).parseFromString(raw, 'text/html');
		let body = doc.querySelector('body');

		return body.innerHTML
	}

	addItem(id, name, content) {
		let html = Content.parseHTML(content);

		let sec, header, lnk, tit, body;
		sec = document.importNode(this.sec);
		header = document.importNode(this.header);
		lnk = document.importNode(this.lnk);
		tit = document.importNode(this.title);
		body = document.importNode(this.body);

		sec.id = id;
		lnk.setAttribute('href', '#' + id);
		tit.innerText = name;
		body.innerHTML = html;

		header.appendChild(tit);
		header.appendChild(lnk);
		sec.appendChild(header);
		sec.appendChild(body);
		this.module.appendChild(sec);
		return this
	}
}
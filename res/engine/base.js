class Loader {
	constructor(file) {
		this.file = file
	}

	async loadJSON() {
		try {
			const res = await fetch(`/db/${this.file}.json`)
			return res.json()
		} catch (e) {
			throw e
		}
	}

}

class Render {
	constructor(base = 'main') {
		this.baseName = base
		this.base = document.querySelector(base)
    this.components = []
	}

	load(...components) {
		this.components = this.components.concat(components)
		return this
	}

	start() {
		Array.isArray(this.components)
			? this.components.forEach(component => this.base.appendChild(component))
			: this.base.appendChild(this.components)
	}
}

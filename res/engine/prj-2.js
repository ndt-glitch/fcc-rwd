// codepen.io/dangtu/pen/MqXEmb
// w3schools.com/howto/howto_js_form_steps.asp

class Content {
	constructor(db) {
		// Fetch placeholder to insert data
		this.animeList = document.querySelector('#animeList')
		this.dropdown = document.querySelector('#dropdown')

		this.db = db
		this.data = {}
	}


	addAnimeList() {
		var id = 'name-' + this.data.id;
		var title = this.data.title;

		var wrap = document.createElement('label');
		var input = document.createElement('input');
		var label = document.createElement('span');
		var node = document.createTextNode(title);

		wrap.classList.add('inline-log');
		wrap.setAttribute('for', id);

		input.setAttribute('name', 'anime');
		input.setAttribute('type', 'checkbox');
		input.setAttribute('value', title);
		input.setAttribute('id', id);

		label.appendChild(node);

		wrap.appendChild(input);
		wrap.appendChild(label);
		this.animeList.appendChild(wrap);
	}

	addDropDown() {
		var title = this.data.title;

		var option = document.createElement('option');
		var node = document.createTextNode(title);

		option.setAttribute('value', title);

		option.appendChild(node);
		this.dropdown.appendChild(option);
	}

	draw() {
		for (this.data of this.db) {
			this.addAnimeList();
			this.addDropDown();
		}
	}
}

class Section {
	constructor() {
		this.section = document.querySelectorAll('section');
		this.size = this.section.length;
	}

	get activePos() {
		for (var i = 0; i < this.size; i++) {
			if (this.section[i].classList.contains('active')) return i
		}
	}
}

// Require hr#process
class Process extends Section {
	constructor() {
		super();
		this.bar = document.querySelector('#process');
		this.percent;
	}

	draw() {
		var max = this.size - 1;
		var cur = this.activePos; // Avoid 0 pos of array

		this.percent = cur * (100 / max);
		this.percent += '%';

		this.bar.style.width = this.percent;
	}
}

// Require button.btn
class Nav extends Section {
	constructor() {
		super();
		this.back = document.querySelector('#back');
		this.next = document.querySelector('#next');
		this.send = document.querySelector('#submit');
		this.process = new Process();

		this.label = {
			back: 'Back',
			view: 'Preview',
			skip: 'Skip',
			next: 'Next'
		};
	}

	addLabel(label) {
		/* Good promgrammer will check if label belongs to Button.label */
		/* Guess I'm a black sheep anyway */
		switch (label) {
			case 'Back':
			case 'Preview':
				this.back.innerHTML = label;
				break;

			case 'Skip':
			case 'Next':
				this.next.innerHTML = label;
				break;
		}
	}

	checkVaild() {
		var sect = this.section[this.activePos];
		var input = sect.querySelectorAll('input[required]');
		var select = sect.querySelectorAll('select[required]');
		var textarea = sect.querySelectorAll('textarea[required]');

		console.log(input);

		// working on it?
	}

	checkPos() {
		var max = this.size - 1;
		//console.log(`${this.activePos}/${max}`);

		this.process.draw();

		if (this.activePos === max) {
			this.addLabel(this.label.view);
			this.next.classList.add('inactive');
			this.send.classList.remove('inactive');
		}
		else if (this.activePos === 0) {
			this.addLabel(this.label.back);
			this.addLabel(this.label.next);

			this.back.disabled = true;
			// TODO disable the next.btn if required fields haven't been filled
		}
		else {
			this.addLabel(this.label.back);
			// TODO check if section has no required field > this.addLabel(this.label.skip);

			this.back.disabled = false;
		}
	}

	jumpTo(n) {
		var cur = this.activePos;
		var newPos = cur + n;

		this.section[cur].classList.replace('active', 'inactive');
		this.section[newPos].classList.replace('inactive', 'active')
	}

	goBack() {
		this.jumpTo(-1)
	}

	goNext() {
		this.checkVaild();
		this.jumpTo(1)
	}

	preview() {
		var classes = document.querySelectorAll('section.inactive');

		for (var aClass of classes)
			aClass.classList.remove('inactive');

		for (var i = 1; i < this.section.length; i++)
			this.section[i].style.marginTop = '25pt';

		// Remove the review.btn
		this.back.parentNode.removeChild(this.back);
		this.send.classList.replace('right', 'mono')
	}

	run() {
		this.checkPos();

		this.back.onclick = () => {
			if (this.activePos === (this.size - 1))
				this.preview();
			else
				this.goBack();

			this.checkPos();

			return false
		}

		this.next.onclick = () => {
			this.goNext();
			this.checkPos();

			return false
		}
	}
};
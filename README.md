# Welcome to RWD

RWD - Responsive Web Design - project was built for my [freeCodeCamp][freecodecamp] [certification][certification].

I didn't use server script in this RWD project because I want to practice pure client Js. Everything was executed right on client script.

This project is open for public (not open for business - LOL) under MIT license. However, I strongly recommend not to look at my project until you finish yours. I share it just as a reference not a copy for passing the test.

## Moving on Glitch?

Glitch actually provides a full playground for FE and BE. That's why I decided to move here. Even for a static site project like RWD, using a bit power from BE is a good idea. With it, I could organize project better and visitors won't get lost in a bunch of stuffs.

All of my source code was kept in Codepen.io in past. However, Codepen doesn't seem like a good place to ogranize my stuffs. So after found out the existen of Glitch, I decided to move my source code to here and improve it.

From here, I will use `fetch` API to get JSON from server. It's way more fun than using object-array.

## Bugs

...


# Credit
- Glitch project icon from [icons-for-free.com](https://icons-for-free.com/mobile+mobile+friendly+responsive+tablet+icon-1320196719997071733)


# Contact

You can [contact me via Twitter][twitter] in case you have question for my project, complain copyright, or suggestion to improve the project.

[freecodecamp]: https://www.freecodecamp.org
[certification]: https://www.freecodecamp.org/certification/dangtu/responsive-web-design
[glitch]: https://glitch.com/~dt-rwd
[twitter]: https://twitter.com/ngdangtu
